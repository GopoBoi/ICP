.PHONY: all clean

all: sim

run: clean sim
	exec ./build/ICP_01

sim:
	mkdir build
	qmake ./src -o ./build/Makefile
	make -C ./build

doxygen: 
	doxygen

pack: clean
	zip -r xmalic02-xjuras11.zip *

clean:
	rm -rdf build doc *.zip
