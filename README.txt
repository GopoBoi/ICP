# ICP_01
Program that simulates robots that move automatically.

## Functionality
Robots can be edited, switched to mouse control mode, moved and selected.
The simulation can be paused a played, it runs at 10 ms per step.
Obstacles can be placed, selected and moved.
Individual scenarios can be saved and loaded.

## Authors
- Jan Malicek (xmalic02)
- Martin Jurasek (xjuras11)

