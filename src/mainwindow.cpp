/**
 * @file mainwindow.cpp
 * @brief Implementation of the main window of the program.
 * @authors Jan Malicek (xmalic02), Martin Jurasek (xjuras11)
 *
 * This class holds all other widgets and scenes of the program. It also handles controls with widgets and shortcuts.
 */

#include "mainwindow.hpp"

/**
 * @brief Constructor
 */
MainWindow::MainWindow() {
    CreateActions();
    CreateToolBar();
    CreateInspector();

    addToolBar(Qt::LeftToolBarArea, &tool_bar);

    //scene set-up
    scene.setItemIndexMethod(QGraphicsScene::NoIndex);
    scene.setSceneRect(0, 0, 2048, 2048);
    scene.setBackgroundBrush(QColor(50, 50, 50));

    //simulation set-up
    simulation.setScene(&scene);
    simulation.setRenderHint(QPainter::Antialiasing);
    simulation.setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    simulation.setWindowTitle("Robot Simulation");

    //window layout
    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(&simulation);

    //add layout to window
    QWidget *widget = new QWidget;
    widget->setLayout(layout);
    setCentralWidget(widget);

    //if selection changes, we need to check if inspector needs to be shown
    connect(&scene, &QGraphicsScene::selectionChanged, this, &MainWindow::SelectionChanged);
}
/**
 * @brief Creates actions for the toolbar
 */
void MainWindow::CreateActions() {
    ModeGroup = new QActionGroup(this);

    //create all toolbar buttons
    QAction *MoveAction = new QAction("Move");
    MoveAction->setCheckable(true);
    MoveAction->setShortcut(Qt::Key_1);
    MoveAction->setToolTip("Shortcut: 1");
    ModeGroup->addAction(MoveAction);
QAction *SelectAction = new QAction("Select");
    SelectAction->setCheckable(true);
    SelectAction->setShortcut(Qt::Key_2);
    SelectAction->setToolTip("Shortcut: 2");
    ModeGroup->addAction(SelectAction);

    QAction *RobotAction = new QAction("Robot");
    RobotAction->setCheckable(true);
    RobotAction->setShortcut(Qt::Key_3);
    RobotAction->setToolTip("Shortcut: 3");
    ModeGroup->addAction(RobotAction);

    QAction *WallAction = new QAction("Wall");
    WallAction->setCheckable(true);
    WallAction->setShortcut(Qt::Key_4);
    WallAction->setToolTip("Shortcut: 4");
    ModeGroup->addAction(WallAction);
    ModeGroup->setExclusive(true);

    PlayAction = new QAction("Play");
    PlayAction->setShortcut(Qt::Key_Space);
    PlayAction->setToolTip("Shortcut: Space");
    PlayAction->setCheckable(true);

    QAction *NewAction = new QAction("New Scenario");
    QAction *SaveAction = new QAction("Save Scenario");
    QAction *LoadAction = new QAction("Load Scenario");
    QAction *QuitAction = new QAction("Quit");

    //create file menu
    QMenu *file_menu = menuBar()->addMenu("Scenario");
    file_menu->addAction(NewAction);
    file_menu->addAction(SaveAction);
    file_menu->addAction(LoadAction);
    file_menu->addAction(QuitAction);

    //connect button actions to their functions
    connect(MoveAction, &QAction::triggered, &simulation, &Simulation::setMove);
    connect(SelectAction, &QAction::triggered, &simulation, &Simulation::setSelect);
    connect(RobotAction, &QAction::triggered, &simulation, &Simulation::setRobot);
    connect(WallAction, &QAction::triggered, &simulation, &Simulation::setWall);
    connect(PlayAction, &QAction::triggered, &simulation, &Simulation::PlayPause);

    connect(NewAction, &QAction::triggered, &simulation, &Simulation::newScenario);
    connect(SaveAction, &QAction::triggered, &simulation, &Simulation::saveScenario);
    connect(LoadAction, &QAction::triggered, &simulation, &Simulation::loadScenario);
    connect(QuitAction, &QAction::triggered, this, &MainWindow::close);
}
/**
 * @brief Creates the toolbar
 */
void MainWindow::CreateToolBar() {
    tool_bar.addActions(ModeGroup->actions());
    tool_bar.addSeparator();
    tool_bar.addAction(PlayAction);
}

/**
 * @brief Creates the robot inspector widget.
 */
void MainWindow::CreateInspector() {
    //create inspector
    inspector = new QDockWidget(this);
    addDockWidget(Qt::RightDockWidgetArea, inspector);

    QLabel *inspector_label = new QLabel(inspector);
    inspector_label->setText("Robot Properties");

    QWidget *widget = new QWidget;

    //vertical layout for inspector
    QVBoxLayout *layout = new QVBoxLayout(widget);
    layout->setAlignment(Qt::AlignTop);


    //create inspector widgets
    radius_box = new QDoubleSpinBox(widget);
    radius_box->setMinimum(10.0);
    radius_box->setMaximum(100.0);
    radius_box->setSingleStep(5.0);
    QLabel *radius_label = new QLabel(widget);
    radius_label->setText("Radius");
    radius_label->setBuddy(radius_box);

    detection_box = new QDoubleSpinBox(widget);
    detection_box->setMaximum(500.0);
    detection_box->setSingleStep(10.0);
    QLabel *detect_label = new QLabel(widget);
    detect_label->setText("Detection distance");
    detect_label->setBuddy(detection_box);

    speed_box = new QDoubleSpinBox(widget);
    speed_box->setMaximum(50.0);
    speed_box->setSingleStep(0.5);
    QLabel *speed_label = new QLabel(widget);
    speed_label->setText("Move speed");
    speed_label->setBuddy(speed_box);

    turn_angle_box = new QDoubleSpinBox(widget);
    turn_angle_box->setMaximum(30.0);
    turn_angle_box->setSingleStep(1.0);
    QLabel *t_angle_label = new QLabel(widget);
    t_angle_label->setText("Turn speed");
    t_angle_label->setBuddy(turn_angle_box);

    turn_direction = new QCheckBox(widget);
    QLabel *t_direction_label = new QLabel(widget);
    t_direction_label->setText("Turn Clockwise");
    t_direction_label->setBuddy(turn_direction);

    control = new QCheckBox(widget);
    QLabel *control_label = new QLabel(widget);
    control_label->setText("Control using mouse");
    control_label->setBuddy(control);

    color_button = new QPushButton("Change color", widget);

    //add widgets to inspector
    layout->addWidget(radius_label);
    layout->addWidget(radius_box);
    layout->addWidget(detect_label);
    layout->addWidget(detection_box);
    layout->addWidget(speed_label);
    layout->addWidget(speed_box);
    layout->addWidget(t_angle_label);
    layout->addWidget(turn_angle_box);
    layout->addWidget(t_direction_label);
    layout->addWidget(turn_direction);
    layout->addWidget(control_label);
    layout->addWidget(control);
    layout->addWidget(color_button);

    widget->setLayout(layout);

    inspector->setWidget(widget);
    inspector->hide();
}

/**
 * @brief Connects the inspector to a robot and makes it visible
 *
 * @param robot the instance of Robot to connect to
 */
void MainWindow::ShowInspector(Robot *robot) {
    //get current values from robot
    radius_box->setValue(robot->getRadius());
    detection_box->setValue(robot->getLength());
    speed_box->setValue(robot->getSpeed());
    turn_angle_box->setValue(robot->getTurnAngle());
    turn_direction->setChecked(robot->getTurnDirection());
    control->setChecked(robot->getControl());

    //connect all control elements to robot
    connect(radius_box, QOverload<qreal>::of(&QDoubleSpinBox::valueChanged), [=](qreal i){robot->changeRadius(i);});
    connect(speed_box, QOverload<qreal>::of(&QDoubleSpinBox::valueChanged), [=](qreal i){robot->changeSpeed(i);});
    connect(turn_angle_box, QOverload<qreal>::of(&QDoubleSpinBox::valueChanged), [=](qreal i){robot->changeTurnAngle(i);});
    connect(detection_box, QOverload<qreal>::of(&QDoubleSpinBox::valueChanged), [=](qreal i){robot->changeDetection(i);});
    connect(turn_direction, &QCheckBox::clicked, robot, &Robot::changeTurnDirection);
    connect(control, &QCheckBox::clicked, robot, &Robot::changeControl);
    connect(color_button, &QPushButton::clicked, robot, &Robot::changeColor);

    //show the inspector
    inspector->show();
}

/**
 * @brief Checks if a robot is selected when selection changes.
 */
void MainWindow::SelectionChanged() {
    //selection changed, disconnect all control elements
    radius_box->disconnect();
    speed_box->disconnect();
    detection_box->disconnect();
    turn_angle_box->disconnect();
    turn_direction->disconnect();
    control->disconnect();
    color_button->disconnect();

    //selection changed, hide inspector
    inspector->hide();

    //go through all selected items
    for (auto object : scene.selectedItems()) {
        //If robot(s) selected, show inspector
        if (object->type() == Robot::UserType) {
            ShowInspector((Robot *)object);
        }
    }
}
