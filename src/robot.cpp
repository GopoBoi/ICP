/**
 * @file robot.cpp
 * @brief Robot class implementation
 * @authors Jan Malicek (xmalic02), Martin Jurasek (xjuras11)
 */

#include "robot.hpp"

/**
 * @brief Constructor
 *
 * @param parent the Robot instance that uses this detection cone
 * @param radius radius of the Robot parent
 * @param length distance at which robot should detect obstacles
 */
DetectionCone::DetectionCone(QGraphicsObject *parent, qreal radius, qreal length) {
        setParentItem(parent);
        //create cone shape
        QPolygonF temp_polygon;
        temp_polygon << QPointF(0, radius)
                     << QPointF(0, -radius)
                     << QPointF(length, -radius * sqrt(length / 50))
                     << QPointF(length, radius * sqrt(length / 50));
        setPolygon(temp_polygon);
        //make sure robot is painted above
        setFlag(QGraphicsItem::ItemStacksBehindParent);
}

QPainterPath DetectionCone::shape() const {
    QPainterPath path;
    path.addPolygon(polygon());
    return path;
}

/**
 * @brief paints the detection cone
 *
 */
void DetectionCone::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
    painter->setBrush(QColor(255, 0, 0, 80));
    painter->setPen(Qt::NoPen);
    painter->drawPolygon(polygon());
    painter->setBrush(Qt::NoBrush);
}

/**
 * @brief changes the shape of the detection cone
 *
 * @param radius new robot radius
 * @param length new detection distance
 */
void DetectionCone::detectionChange(qreal radius, qreal length) {
    QPolygonF temp_polygon;
    temp_polygon << QPointF(0, radius)
                 << QPointF(0, -radius)
                 << QPointF(length, -radius * sqrt(length / 50))
                 << QPointF(length, radius * sqrt(length / 50));
    setPolygon(temp_polygon);
}

/**
 * @brief Constructor
 *
 * @param scene The QGraphicsScene to bind to
 */
Robot::Robot(QGraphicsScene *scene) {
    scene->addItem(this);
    this->setFlag(QGraphicsItem::ItemIsSelectable);
    this->setFlag(QGraphicsItem::ItemIsMovable);
}

/**
 * @brief Constructor - constructs a random robot at position
 *
 * @param scene The QGraphicsScene to bind to
 * @param timer The QTimer to connect to
 * @param position Position at which to construct Robot
 */
Robot::Robot(QGraphicsScene *scene, QTimer *timer, QPointF position) {
    scene->addItem(this);

    _radius = 30;

    _angle = 0;
    setRotation(-_angle);

    _speed = 10;
    _turn_angle = _speed / 2;
    _turn_direction = false;
    _color = QColor(qrand() % 256, qrand() % 256, qrand() % 256);

    _detection_length = 120;
    _detection_cone = new DetectionCone(this, _radius, _detection_length);

    this->setFlag(QGraphicsItem::ItemIsSelectable);
    this->setFlag(QGraphicsItem::ItemIsMovable);

    setPos(position);
    connect(timer, &QTimer::timeout, this, &Robot::Move);
}

/**
 * @brief returns bounding rectangle of robot
 */
QRectF Robot::boundingRect() const {
    return QRectF(-_radius, -_radius, _radius * 2, _radius * 2);
}

QPainterPath Robot::shape() const {
    QPainterPath path;
    path.addEllipse(boundingRect());
    return path;
}

/**
 * @brief Paints the robot to the screen
 *
 */
void Robot::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
    if (this->isSelected())
        painter->setPen(Qt::white);
    else
        painter->setPen(Qt::black);
    painter->setBrush(_color);
    painter->drawEllipse(boundingRect());
    painter->drawLine(0, 0, _radius, 0);
    painter->setBrush(Qt::NoBrush);
}


/**
 * @brief handles mouse movements
 *
 * if robot isn't in mouse control mode, passes the mouse event to the base class
 */
void Robot::mouseMoveEvent(QGraphicsSceneMouseEvent *event) {
    if (_control)
        _mouse_track = mapToScene(event->pos());
    else
        QGraphicsItem::mouseMoveEvent(event);
}

/**
 * @brief handles mouse button press
 *
 * if robot isn't in mouse control mode, passes the mouse event to the base class
 */
void Robot::mousePressEvent(QGraphicsSceneMouseEvent *event) {
    if (_control) {
        _mouse_pressed = true;
    }
    else
        QGraphicsItem::mousePressEvent(event);
}

/**
 * @brief handles mouse button release
 *
 * if robot isn't in mouse control mode, passes the mouse event to the base class
 * selects the current robot, so that it is alway selected and control mode can be switched to automatic
 */
void Robot::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
    if (_control) {
        _mouse_pressed = false;
        setSelected(true);
    }
    else
        QGraphicsItem::mouseReleaseEvent(event);
}

/**
 * @brief Writes Robot properties to json
 *
 * @param json The Json object to write to
 */
void Robot::write(QJsonObject &json) const{
    json["pos.x"] = this->pos().x();
    json["pos.y"] = this->pos().y();
    json["radius"] = _radius;
    json["angle"] = _angle;
    json["length"] = _detection_length;
    json["turn.angle"] = _turn_angle;
    json["turn.dir"] = _turn_direction;
    json["speed"] = _speed;
    json["color.r"] = _color.red();
    json["color.g"] = _color.green();
    json["color.b"] = _color.blue();
}

/**
 * @brief Reads Robot properties from json
 *
 * @param json Json Object from which to read
 * @param timer QTimer to connect to
 */
void Robot::read(const QJsonObject &json, QTimer *timer) {
    if (json.contains("pos.x"))
        setX(json["pos.x"].toDouble());

    if (json.contains("pos.y"))
        setY(json["pos.y"].toDouble());

    if (json.contains("radius"))
        _radius = json["radius"].toDouble();

    if (json.contains("angle"))
        _angle = json["angle"].toDouble();

    if (json.contains("turn.angle"))
        _turn_angle = json["turn.angle"].toDouble();

    if (json.contains("turn.dir"))
        _turn_direction = json["turn.dir"].toBool();

    if (json.contains("speed"))
        _speed = json["speed"].toDouble();

    if (json.contains("length"))
        _detection_length = json["length"].toDouble();

    if (json.contains("color.r"))
        _color.setRed(json["color.r"].toInt());

    if (json.contains("color.g"))
        _color.setGreen(json["color.g"].toInt());

    if (json.contains("color.b"))
        _color.setBlue(json["color.b"].toInt());

    setRotation(-_angle);
    _detection_cone = new DetectionCone(this, _radius, _detection_length);

    connect(timer, &QTimer::timeout, this, &Robot::Move);
}

/**
 * @brief Moves Robot every 10 ms
 *
 * Moves automatically unless Robot set to be controlled by mouse.
 */
void Robot::Move() {
    //save last position in case moving unsuccesful
    QPointF last_pos = pos();

    //follow mouse if Robot set to mouse control
    if (_control) {
        //get angle to mouse
        auto line = QLineF(this->pos(), _mouse_track);
        _angle = line.angle();
        setRotation(-_angle);

        //move if mouse button pressed down
        if (_mouse_pressed) {
            setPos(mapToParent(_speed / 2, 0));
        }

        //check for collisions
        auto colliders = this->collidingItems();
        for (auto item : colliders) {
            //if we hit an obstacle or a robot return to last_pos
            if (item->type() == QGraphicsRectItem::Type || item->type() == Robot::UserType) {
                setPos(last_pos);
            }
        }

        return;
    }

    //don't move automatically if currently selected
    if (this->isSelected())
        return;

    //check if detection_cone is colliding with anything
    auto obstacles = _detection_cone->collidingItems();
    for (auto item : obstacles) {
        //don't consider yourself an obstacle
        if (item == this)
            continue;
        //if we are about to hit something turn in turn_direction
        if (item->type() == QGraphicsRectItem::Type || item->type() == Robot::UserType) {
            _angle += _turn_direction ? -_turn_angle : _turn_angle;
            break;
        }
    }

    setRotation(-_angle);

    //move forward
    setPos(mapToParent(_speed / 2, 0));

    //check if we are colliding with something
    auto colliders = this->collidingItems();
    for (auto item : colliders) {
        //if we hit a wall or other robot, return to last_pos
        if (item->type() == QGraphicsRectItem::Type || item->type() == Robot::UserType) {
            setPos(last_pos);
        }
    }
}

/**
 * @brief Changes Robot radius to radius
 *
 * @param radius new radius
 */
void Robot::changeRadius(qreal radius) {
    _radius = radius;
    _detection_cone->detectionChange(_radius, _detection_length);
}

/**
 * @brief Changes Robot speed to speed
 *
 * @param speed new speed
 */
void Robot::changeSpeed(qreal speed) {
    _speed = speed;
}

/**
 * @brief Changes Robot turn_angle to angle
 *
 * @param angle new turn_angle
 */
void Robot::changeTurnAngle(qreal angle) {
    _turn_angle = angle;
}

/**
 * @brief switches turn_direction between clockwise and counter-clockwise
 */
void Robot::changeTurnDirection() {
    _turn_direction = !_turn_direction;
}

/**
 * @brief switches between automatic movement and mouse control
 */
void Robot::changeControl() {
    _control = !_control;
    //we don't need a detection cone if manually controlled
    _detection_cone->setVisible(!_control);
    if (_control)
        grabMouse();
    else
        ungrabMouse();
}

/**
 * @brief changes detection distance to length
 *
 * @param length new detection distance
 */
void Robot::changeDetection(qreal length) {
    _detection_length = length;
    //update shape of detection_cone
    _detection_cone->detectionChange(_radius, _detection_length);
}

/**
 * @brief changes color using a color dialog
 */
void Robot::changeColor() {
    //open color picker dialog
    _color = QColorDialog::getColor(_color);
    this->update();
}

/**
 * @brief 
 *
 * @return returns current Robot speed
 */
qreal Robot::getSpeed() {
    return _speed;
}

/**
 * @brief 
 *
 * @return returns current Robot turn angle
 */
qreal Robot::getTurnAngle() {
    return _turn_angle;
}

/**
 * @brief 
 *
 * @return returns current Robot turn direction
 */
bool Robot::getTurnDirection() {
    return _turn_direction;
}

/**
 * @brief 
 *
 * @return returns current Robot control mode
 */
bool Robot::getControl() {
    return _control;
}

/**
 * @brief 
 *
 * @return returns current Robot radius
 */
qreal Robot::getRadius() {
    return _radius;
}

/**
 * @brief 
 *
 * @return returns current Robot detection distance
 */
qreal Robot::getLength() {
    return _detection_length;
}
