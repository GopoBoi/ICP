/**
 * @file main.cpp
 * @brief entry point of the program
 * @authors Jan Malicek (xmalic02), Martin Jurasek (xjuras11)
 */

#include <QApplication>

#include "mainwindow.hpp"

int main(int argc, char *argv[] ) {
    QApplication app(argc, argv);
    MainWindow window;
    window.showFullScreen();
    window.show();

    return app.exec();
}
