/**
 * @file simulation.cpp
 * @brief Simulation class implementation
 * @authors Jan Malicek (xmalic02), Martin Jurasek (xjuras11)
 */


#include "simulation.hpp"

/**
 * @brief Constructor
 */
Simulation::Simulation() {
    setMove();
    temp_rect = nullptr;
    //no scroll bars
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setMouseTracking(true);

    timer = new QTimer(this);
    running = false;
}

void Simulation::mousePressEvent(QMouseEvent *event) {
    if (event->button() == Qt::MouseButton::LeftButton) {
        switch (mode) {
        case MOVE:
            QGraphicsView::mousePressEvent(event);
            break;
        case SELECT:
            QGraphicsView::mousePressEvent(event);
            break;
        case ROBOT:
            //create new random robot at mouse position
            robots.append(new Robot(scene(), timer, mapToScene(event->pos())));
            break;
        case WALL:
            //start drawing rectangle
            temp_rect = new QGraphicsRectItem;
            scene()->addItem(temp_rect);
            drag_start = mapToScene(event->pos());
            break;
        }
    }
}

void Simulation::mouseMoveEvent(QMouseEvent *event) {
    switch (mode) {
    case MOVE:
        QGraphicsView::mouseMoveEvent(event);
        break;
    case SELECT:
        QGraphicsView::mouseMoveEvent(event);
        break;
    case WALL:
        if (temp_rect != nullptr) {
            temp_rect->setRect(QRectF(drag_start, mapToScene(event->pos())));
            scene()->update();
        }
        break;
    }
}
void Simulation::mouseReleaseEvent(QMouseEvent *event) {
    if (event->button() == Qt::MouseButton::LeftButton) {
        switch (mode) {
        case MOVE:
            QGraphicsView::mouseReleaseEvent(event);
            break;
        case SELECT:
            QGraphicsView::mouseReleaseEvent(event);
            break;
        case ROBOT:
            break;
        case WALL:
            if (temp_rect != nullptr) {
                //create new obstacle from mouse drag
                QGraphicsRectItem *new_obstacle = new QGraphicsRectItem(QRectF(drag_start, mapToScene(event->pos())));
                new_obstacle->setBrush(QBrush(QColor(255, 255, 255), Qt::BDiagPattern));
                new_obstacle->setPen(QColor(255, 255, 255));
                new_obstacle->setFlag(QGraphicsItem::ItemIsSelectable);
                new_obstacle->setFlag(QGraphicsItem::ItemIsMovable);
                obstacles.append(new_obstacle);
                scene()->addItem(new_obstacle);
                scene()->removeItem(temp_rect);
                delete temp_rect;
                temp_rect = nullptr;
            }
            break;
        }
    }
}

/**
 * @brief writes the simulation state to json
 *
 * @param json the Json object to write to
 */
void Simulation::write(QJsonObject &json) const {
    //write all robots
    QJsonArray j_robots;
    for (Robot *robot : robots) {
        QJsonObject j_robot;
        robot->write(j_robot);
        j_robots.append(j_robot);
    }
    json["robots"] = j_robots;

    //write all obstacles
    QJsonArray j_obstacles;
    for (auto obs : obstacles) {
        QJsonObject j_obstacle;
        QRectF rect = obs->mapRectToScene(obs->rect());
        j_obstacle["topleft.x"] = rect.topLeft().x();
        j_obstacle["topleft.y"] = rect.topLeft().y();
        j_obstacle["bottomright.x"] = rect.bottomRight().x();
        j_obstacle["bottomright.y"] = rect.bottomRight().y();
        j_obstacles.append(j_obstacle);
    }
    json["obstacles"] = j_obstacles;
}

/**
 * @brief reads and constructs a simulation from json
 *
 * @param json the Json object to read from
 */
void Simulation::read(const QJsonObject &json) {
    newScenario();
    if (json.contains("robots")) {
        QJsonArray j_robots = json["robots"].toArray();
        robots.reserve(j_robots.size());
        for (int i = 0; i < j_robots.size(); ++i) {
            QJsonObject j_robot = j_robots[i].toObject();
            Robot *robot = new Robot(scene());
            robot->read(j_robot, timer);
            robots.append(robot);
        }
    }
    if (json.contains("obstacles")) {
        QJsonArray j_obstacles = json["obstacles"].toArray();
        obstacles.reserve(j_obstacles.size());
        for (int i = 0; i < j_obstacles.size(); ++i) {
            QJsonObject j_obstacle = j_obstacles[i].toObject();

            //read corners of rectangle
            QPointF topLeft = QPointF(j_obstacle["topleft.x"].toDouble(), j_obstacle["topleft.y"].toDouble());
            QPointF bottomRight = QPointF(j_obstacle["bottomright.x"].toDouble(), j_obstacle["bottomright.y"].toDouble());

            //create new obstacle from corners and apply visual effects
            QGraphicsRectItem *obstacle = new QGraphicsRectItem(QRectF(topLeft, bottomRight));
            obstacle->setBrush(QBrush(QColor(255, 255, 255), Qt::BDiagPattern));
            obstacle->setPen(QColor(255, 255, 255));
            //make obstacle movable
            obstacle->setFlag(QGraphicsItem::ItemIsSelectable);
            obstacle->setFlag(QGraphicsItem::ItemIsMovable);
            obstacles.append(obstacle);
            scene()->addItem(obstacle);
        }
    }
}

/**
 * @brief save current simulation into file
 *
 * @return 
 */
bool Simulation::saveScenario() {
    //get file name
    QString file_name = QFileDialog::getSaveFileName(this,
                                                    tr("Save Scenario"), "", tr("Json Files (*.json)"));
    //if user didn't add json extension, do so
    if (!file_name.contains(".json"))
        file_name.append(".json");

    //open file
    QFile save_file(file_name);
    save_file.open(QIODevice::WriteOnly);

    QJsonObject scenario;
    write(scenario);
    save_file.write(QJsonDocument(scenario).toJson());

    return true;
}
/**
 * @brief load simulation from file
 *
 * @return 
 */
bool Simulation::loadScenario() {
    //get file name
    QString file_name = QFileDialog::getOpenFileName(this,
                                            tr("Load Scenario"), "", tr("Json Files (*.json)"));
    //open file
    QFile load_file(file_name);
    load_file.open(QIODevice::ReadOnly);

    //load data
    QByteArray save_data = load_file.readAll();

    //load json
    QJsonDocument load_doc(QJsonDocument::fromJson(save_data));

    read(load_doc.object());
    return true;
}

/**
 * @brief clear current simulation and create a new one
 */
void Simulation::newScenario() {
    //destroy all robots
    for (auto r : qAsConst(robots))
        delete r;
    robots.clear();
    //destroy all obstacles
    for (auto o : qAsConst(obstacles))
        delete o;
    obstacles.clear();
}

/**
 * @brief set cursor into move mode
 */
void Simulation::setMove() {
    setDragMode(ScrollHandDrag);
    setInteractive(false);
    mode = Mode::MOVE;
}
/**
 * @brief set cursor into selection mode
 */
void Simulation::setSelect() {
    setDragMode(RubberBandDrag);
    setInteractive(true);
    mode = Mode::SELECT;
}
/**
 * @brief set cursor in robot placing mode
 */
void Simulation::setRobot() {
    setDragMode(NoDrag);
    mode = Mode::ROBOT;
}
/**
 * @brief set cursor into obstacle placing mode
 */
void Simulation::setWall() {
    setDragMode(NoDrag);
    mode = Mode::WALL;
}
/**
 * @brief toggle playing simulation
 */
void Simulation::PlayPause() {
    if (running)
        timer->stop();
    else
        timer->start(10);
    running = !running;
}
