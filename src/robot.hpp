/**
 * @file robot.hpp
 * @brief header file for robot.cpp
 * @authors Jan Malicek (xmalic02), Martin Jurasek (xjuras11)
 */

#ifndef ROBOT_HPP
#define ROBOT_HPP

#include <QGraphicsObject>
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include <QTimer>
#include <QJsonObject>
#include <QColorDialog>
#include <cmath>

#include "qglobal.h"

/**
 * @class DetectionCone
 * @brief Detection Area for robot.
 *
 */
class DetectionCone : public QGraphicsPolygonItem {
public:
    DetectionCone(QGraphicsObject *parent, qreal radius, qreal length);

    QPainterPath shape() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    void detectionChange(qreal radius, qreal length);
};

/**
 * @class Robot
 * @brief Robot implementation
 *
 */
class Robot : public QGraphicsObject {
private:
    /**
     * @brief current speed of robot
     */
    qreal _speed;
    /**
     * @brief angle of robot view
     */
    qreal _angle;
    /**
     * @brief radius of robot body
     */
    qreal _radius;
    /**
     * @brief how much robot turns per turn
     */
    qreal _turn_angle;
    /**
     * @brief how far away can robot see obstacles
     */
    qreal _detection_length;
    /**
     * @brief the direction of turning of robot
     * @true robot turns clockwise
     * @false robot turns counter-clockwise
     */
    bool _turn_direction;
    /**
     * @brief robot control mode
     * @true mouse control mode
     * @false automatic mode
     */
    bool _control = false;
    /**
     * @brief color of robot body
     */
    QColor _color;
    /**
     * @brief detection cone of robot
     */
    DetectionCone *_detection_cone;

    /**
     * @brief keeps track of mouse position if control is in mouse control mode
     */
    QPointF _mouse_track;
    /**
     * @brief keeps track of mouse button position
     */
    bool _mouse_pressed = false;

public:
    Robot(QGraphicsScene *scene);
    Robot(QGraphicsScene *scene, QTimer *timer, QPointF position);

    QRectF boundingRect() const override;
    QPainterPath shape() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;

    void write(QJsonObject &json) const;
    void read(const QJsonObject &json, QTimer *timer);

    qreal getSpeed();
    qreal getTurnAngle();
    qreal getRadius();
    bool getTurnDirection();
    bool getControl();
    qreal getLength();

public slots:
    void Move();

    void changeRadius(qreal radius);
    void changeSpeed(qreal speed);
    void changeTurnAngle(qreal speed);
    void changeTurnDirection();
    void changeControl();
    void changeDetection(qreal length);
    void changeColor();
};
#endif // ROBOT_HPP
