/**
 * @file mainwindow.hpp
 * @brief header file for mainwindow.cpp
 * @authors Jan Malicek (xmalic02), Martin Jurasek (xjuras11)
 */

#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QAction>
#include <QToolBar>
#include <QGraphicsScene>
#include <QMainWindow>
#include <QMenuBar>
#include <QDockWidget>
#include <QDoubleSpinBox>
#include <QCheckBox>
#include <QLayout>
#include <QLabel>
#include <QPushButton>

#include "simulation.hpp"

/**
 * @class MainWindow
 * @brief The main window of the program
 *
 */
class MainWindow : public QMainWindow {
    /**
     * @brief robot simulation
     */
    Simulation simulation;
    /**
     * @brief scene for graphics
     */
    QGraphicsScene scene;
    /**
     * @brief cursor toolbar
     */
    QToolBar tool_bar;

    /**
     * @brief robot properties widget
     */
    QDockWidget *inspector;

    //inspector widgets
    QDoubleSpinBox *radius_box;
    QDoubleSpinBox *detection_box;
    QDoubleSpinBox *speed_box;
    QPushButton *color_button;
    QDoubleSpinBox *turn_angle_box;
    QCheckBox *turn_direction;
    QCheckBox *control;

    /**
     * @brief toolbar cursor mode buttons
     */
    QActionGroup *ModeGroup;
    /**
     * @brief simulation playback toggle
     */
    QAction *PlayAction;

public:
    MainWindow();
    void CreateActions();
    void CreateToolBar();
    void CreateInspector();
    void ShowInspector(Robot *robot);
public slots:
    void SelectionChanged();
};

#endif // MAINWINDOW_HPP
