/**
 * @file simulation.hpp
 * @brief header file for simulation.cpp
 * @authors Jan Malicek (xmalic02), Martin Jurasek (xjuras11)
 */

#ifndef SIMULATION_HPP
#define SIMULATION_HPP

#include <QMouseEvent>
#include <QGraphicsView>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QFileDialog>

#include "robot.hpp"

/**
 * @class Simulation
 * @brief handles all top level logic of the simulation
 *
 * holds all elements needed for simulation, handles the timer for robot movement, handles control of the simulation.
 */
class Simulation : public QGraphicsView {
public:
    /**
     * @brief point where mouse drag started
     */
    QPointF drag_start;
    /**
     * @brief temporary rectangle display for visual effect
     */
    QGraphicsRectItem *temp_rect;
    /**
     * @brief list of all robots in simulation
     */
    QList<Robot *> robots;
    /**
     * @brief list of all obstacles in simulation
     */
    QList<QGraphicsRectItem *> obstacles;
    /**
     * @brief timer for robot movement
     */
    QTimer *timer;
    /**
     * @brief state of the timer
     */
    bool running;

    /**
     * @brief cursor mode
     */
    enum Mode {
        MOVE,
        SELECT,
        ROBOT,
        WALL,
    };
    /**
     * @brief current cursor mode
     */
    Mode mode;

    Simulation();
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void read(const QJsonObject &json);
    void write(QJsonObject &json) const;

public slots:
    void setMove();
    void setSelect();
    void setRobot();
    void setWall();
    void PlayPause();
    void newScenario();
    bool saveScenario();
    bool loadScenario();
};

#endif // SIMULATION_HPP
